# Collections

Your flexible organizer.

This software lets you organize your things according to properties that you can define yourself, and
then it lets you search and query everthing based on these properties.

First, you need to define some content. For example, to store photos, you might define a file object:

```
object file {
    path: path,
}
```

Next, you might define a *photo* object:

```
object photo: file {
    taken: date,
}
```

What this syntax means is that anything that is a file already can also be a photo, but it needs
the `taken` field.

## Builtin Primitives

- path
- data
- hash
- location
- date
- time
- datetime
- number
- string

## Object syntax

Product type

```
object <name>: <dependency1>, <dependency2>, ... {
    field: type,
    optional_field: type?,
    array_field: [type],
    array_field_fixed: [type; 18],
    array_field_fixed_range: [type; 7..19],
    enum_field: enum {
        case: type,
        case: type,
    }
}
```
    

## TODO

- Parser for object, struct and enum syntax
- Use sea-query to emit SQL table create calls for each definition
- Uniqueness and indices?
- How to add?
- How to query?
