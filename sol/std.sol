module std;

struct Path {
    value: builtin::Text,
}

impl Path {
    fn is_absolute(self) {
        self.value.like("/%")
    }

    fn is_relative(self) {
        !self.is_absolute()
    }
}
