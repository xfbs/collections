use std::collections::BTreeMap;
use std::ops::Range;

#[derive(Clone, Debug)]
pub struct ObjectDef {
    pub name: String,
    pub object: Object,
}

#[derive(Clone, Debug)]
pub struct Object {
    pub dependencies: Vec<String>,
    pub fields: BTreeMap<String, Type>,
}

#[derive(Clone, Debug)]
pub struct StructDef {
    pub name: String,
    pub value: Struct,
}

#[derive(Clone, Debug)]
pub struct Struct {
    pub fields: BTreeMap<String, Type>,
}

#[derive(Clone, Debug)]
pub enum Definition {
    Object(Object),
    Struct(Struct),
}

#[derive(Clone, Debug)]
pub struct Field {
    pub name: String,
    pub field_type: Type,
}

#[derive(Clone, Debug)]
pub enum Type {
    /// Unit type
    Nil,
    /// Simple type
    Builtin(BuiltinType),
    /// Enum type
    Enum(Enum),
    /// Struct type
    Struct(Struct),
    /// Optional type
    Option(Box<Type>),
    /// Multiple, ordered instances.
    Array(Box<Type>, Option<Range<usize>>),
    /// Fixed amount
    Tuple(Vec<Type>),
    /// Key-value
    Map(Box<Type>, Box<Type>),
    /// Set
    Set(Box<Type>),
}

#[derive(Clone, Debug)]
pub enum NamedType {
    Builtin(BuiltinType),
    Other(String),
}

#[derive(Clone, Debug)]
pub enum BuiltinType {
    Integer(Option<Range<i64>>),
    Float,
    Bool,
    Path,
    Hash,
    Binary,
    String,
    Location,
    Date,
    Time,
    DateTime,
}

#[derive(Clone, Debug)]
pub struct Enum {
    pub variants: BTreeMap<String, Type>,
}

#[derive(Clone, Debug)]
pub struct EnumDef {
    pub name: String,
    pub def: Enum,
}
