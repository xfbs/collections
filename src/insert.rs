use crate::iden::IdenString;
use crate::types::{BuiltinType, ObjectDef, Type};
use sea_query::{InsertStatement, Value as SeaValue};
use serde_json::{Map, Value};
use std::collections::BTreeMap;

fn as_value(value: &Value) -> SeaValue {
    use Value::*;
    match value {
        Bool(value) => SeaValue::Bool(Some(*value)),
        String(value) => SeaValue::String(Some(Box::new(value.clone()))),
        Number(value) if value.is_i64() => SeaValue::BigInt(Some(value.as_i64().unwrap())),
        Number(value) => SeaValue::Double(Some(value.as_f64().unwrap())),
        _ => unimplemented!(),
    }
}

impl ObjectDef {
    pub fn insert_root(&self, fields: &Map<String, Value>) -> Vec<InsertStatement> {
        let mut insert = InsertStatement::new();
        insert.into_table(IdenString(format!("object:{}", self.name)));
        let mut values = BTreeMap::new();
        let mut statements: Vec<InsertStatement> = vec![];
        for (name, type_def) in &self.object.fields {
            let value = fields.get(name).unwrap();
            for (name, value) in type_def.insert_values(name, value) {
                values.insert(name, value);
            }
            statements.append(&mut type_def.insert_statements(name, value));
        }
        insert.columns(values.keys().map(|c| IdenString(c.clone())));
        insert.values(values.values().map(|c| as_value(c))).unwrap();
        let mut insert = vec![insert];
        insert.append(&mut statements);
        insert
    }

    pub fn insert_leaf(&self, _parent: i64, _fields: &Map<String, Value>) -> Vec<InsertStatement> {
        vec![]
    }
}

impl Type {
    pub fn insert_values(&self, name: &str, value: &Value) -> BTreeMap<String, Value> {
        let field = format!("field:{name}");
        let mut values = BTreeMap::new();
        use Type::*;
        match self {
            Builtin(BuiltinType::String) => {
                values.insert(field, value.clone());
            }
            Builtin(BuiltinType::Integer(_)) => {
                values.insert(field, value.clone());
            }
            Map(_, _) | Array(_, _) | Set(_) | Nil => {}
            _ => unimplemented!(),
        };
        values
    }

    pub fn insert_statements(&self, _name: &str, _value: &Value) -> Vec<InsertStatement> {
        use Type::*;
        match self {
            Builtin(_) | Nil => vec![],
            _ => unimplemented!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::Object;
    use sea_query::SqliteQueryBuilder;
    use serde_json::json;

    #[test]
    fn can_insert_root_string() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [("name".to_string(), Type::Builtin(BuiltinType::String))].into(),
            },
        };
        let values = json!({"name": "Thomas"});
        let insert = object.insert_root(values.as_object().unwrap());
        assert_eq!(
            insert[0].to_string(SqliteQueryBuilder),
            "INSERT INTO \"object:net.xfbs.Image\" (\"field:name\") VALUES ('Thomas')"
        );
    }

    #[test]
    fn can_insert_root_integer() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "number".to_string(),
                    Type::Builtin(BuiltinType::Integer(None)),
                )]
                .into(),
            },
        };
        let values = json!({"number": 12345});
        let insert = object.insert_root(values.as_object().unwrap());
        assert_eq!(
            insert[0].to_string(SqliteQueryBuilder),
            "INSERT INTO \"object:net.xfbs.Image\" (\"field:number\") VALUES (12345)"
        );
    }
}
