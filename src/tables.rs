use crate::iden::IdenString;
use crate::types::{BuiltinType, ObjectDef, Type};
use sea_query::{ColumnDef, ForeignKey, ForeignKeyAction, Table, TableCreateStatement};

impl ObjectDef {
    pub fn table_create(&self) -> Vec<TableCreateStatement> {
        let mut table = Table::create();
        let table_name = format!("object:{}", self.name);
        table.table(IdenString(table_name.clone()));
        table.col(
            ColumnDef::new(IdenString("id".into()))
                .integer()
                .primary_key()
                .not_null(),
        );
        for dependency in &self.object.dependencies {
            let column_name = format!("dependency:{dependency}");
            table.col(
                ColumnDef::new(IdenString(column_name.clone()))
                    .integer()
                    .not_null(),
            );
            table.foreign_key(
                ForeignKey::create()
                    .name("foreign_key")
                    .from(
                        IdenString(table_name.clone()),
                        IdenString(column_name.clone()),
                    )
                    .to(
                        IdenString(format!("object:{dependency}")),
                        IdenString("id".into()),
                    )
                    .on_delete(ForeignKeyAction::Cascade),
            );
        }
        for (name, field) in &self.object.fields {
            for mut column in field.column_def(&name) {
                table.col(&mut column);
            }
        }
        let mut tables = vec![table];
        for (name, field) in &self.object.fields {
            for table in field.table_create(&table_name, &name) {
                tables.push(table);
            }
        }
        tables
    }
}

fn table_for_types(
    parent: &str,
    name: &str,
    key: Option<&Type>,
    value: Option<&Type>,
) -> Vec<TableCreateStatement> {
    let table_name = format!("{parent}:{name}");
    let mut table = Table::create();
    table.table(IdenString(table_name));

    // create foreign key to parent table
    table.col(
        ColumnDef::new(IdenString("object".into()))
            .integer()
            .not_null(),
    );

    if let Some(key) = key {
        // create columns for key
        for mut column in key.column_def("key") {
            table.col(&mut column);
        }
    } else {
        // add index
        table.col(
            ColumnDef::new(IdenString("index".into()))
                .integer()
                .not_null(),
        );
    }

    // add columns for value
    if let Some(value) = value {
        for mut column in value.column_def("value") {
            table.col(&mut column);
        }
    }

    vec![table]
}

impl Type {
    pub fn table_create(&self, table: &str, field: &str) -> Vec<TableCreateStatement> {
        use Type::*;
        match self {
            Array(item_type, _) => table_for_types(table, field, None, Some(&item_type)),
            Map(key_type, value_type) => {
                table_for_types(table, field, Some(&key_type), Some(&value_type))
            }
            Set(item_type) => table_for_types(table, field, Some(&item_type), None),
            Builtin(_) | Option(_) | Nil => vec![],
            // FIXME: create table for some of these subtypes
            Struct(_) | Tuple(_) | Enum(_) => vec![],
        }
    }

    pub fn column_def(&self, name: &str) -> Vec<ColumnDef> {
        self.column_def_inner(name, false)
    }

    pub fn column_def_inner(&self, name: &str, optional: bool) -> Vec<ColumnDef> {
        use Type::*;
        let column_name = format!("field:{name}");
        let mut column_def = ColumnDef::new(IdenString(column_name));
        match self {
            Builtin(BuiltinType::String) => column_def.string(),
            Builtin(BuiltinType::Integer(_)) => column_def.integer(),
            Builtin(BuiltinType::Float) => column_def.double(),
            Builtin(_) => unimplemented!(),
            Option(inner) => return inner.column_def_inner(name, true),
            Tuple(items) => {
                return items
                    .iter()
                    .enumerate()
                    .map(|(num, item)| item.column_def_inner(&format!("{name}:{num}"), optional))
                    .flatten()
                    .collect();
            }
            Enum(inner) => {
                column_def.integer();
                if !optional {
                    column_def.not_null();
                }
                let mut columns = vec![column_def];
                for (variant_name, variant_type) in &inner.variants {
                    let column_name = format!("{name}:{variant_name}");
                    for column in variant_type.column_def_inner(&column_name, true) {
                        columns.push(column);
                    }
                }
                return columns;
            }
            Struct(inner) => {
                let mut columns = vec![];
                for (field_name, field) in &inner.fields {
                    let field_name = format!("{name}:{field_name}");
                    for column in field.column_def_inner(&field_name, optional) {
                        columns.push(column);
                    }
                }
                return columns;
            }
            Nil | Array(_, _) | Set(_) | Map(_, _) => return vec![],
        };

        if !optional {
            column_def.not_null();
        }

        vec![column_def]
    }
}

#[cfg(test)]
mod tests {
    use crate::types::*;
    use sea_query::SqliteQueryBuilder;
    use std::collections::BTreeMap;

    #[test]
    fn can_create_table_single_dependency() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec!["file".into()],
                fields: BTreeMap::new(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"dependency:file\" integer NOT NULL,",
                "FOREIGN KEY (\"dependency:file\") REFERENCES \"object:file\" (\"id\") ON DELETE CASCADE )",
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_string_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [("name".to_string(), Type::Builtin(BuiltinType::String))].into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" text NOT NULL )"
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_optional_string_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Option(Box::new(Type::Builtin(BuiltinType::String))),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" text )"
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_optional_integer_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Option(Box::new(Type::Builtin(BuiltinType::Integer(None)))),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" integer )"
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_integer_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Builtin(BuiltinType::Integer(None)),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" integer NOT NULL )"
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_float_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [("name".to_string(), Type::Builtin(BuiltinType::Float))].into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" real NOT NULL )"
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_optional_float_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Option(Box::new(Type::Builtin(BuiltinType::Float))),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" real )",
            ]
            .join(" ")
        );
    }

    #[test]
    fn can_create_table_tuple_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Tuple(vec![
                        Type::Builtin(BuiltinType::String),
                        Type::Builtin(BuiltinType::Integer(None)),
                    ]),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name:0\" text NOT NULL,",
                "\"field:name:1\" integer NOT NULL )",
            ]
            .join(" "),
        );
    }

    #[test]
    fn can_create_table_optional_tuple_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Option(Box::new(Type::Tuple(vec![
                        Type::Builtin(BuiltinType::String),
                        Type::Builtin(BuiltinType::Integer(None)),
                    ]))),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name:0\" text,",
                "\"field:name:1\" integer )",
            ]
            .join(" "),
        );
    }

    #[test]
    fn can_create_table_tuple_optional_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Tuple(vec![
                        Type::Builtin(BuiltinType::String),
                        Type::Option(Box::new(Type::Builtin(BuiltinType::Integer(None)))),
                    ]),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name:0\" text NOT NULL,",
                "\"field:name:1\" integer )",
            ]
            .join(" "),
        );
    }

    #[test]
    fn can_create_table_array_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Array(Box::new(Type::Builtin(BuiltinType::String)), None),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY",
                ")"
            ]
            .join(" "),
        );
        assert_eq!(
            object
                .table_create()
                .get(1)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image:name\" (",
                "\"object\" integer NOT NULL,",
                "\"index\" integer NOT NULL,",
                "\"field:value\" text NOT NULL )"
            ]
            .join(" "),
        );
    }

    #[test]
    fn can_create_table_enum_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "name".to_string(),
                    Type::Enum(Enum {
                        variants: [
                            ("success".into(), Type::Builtin(BuiltinType::Float)),
                            ("error".into(), Type::Builtin(BuiltinType::String)),
                        ]
                        .into(),
                    }),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:name\" integer NOT NULL,",
                "\"field:name:error\" text,",
                "\"field:name:success\" real )",
            ]
            .join(" "),
        );
    }

    #[test]
    fn can_create_table_struct_field() {
        let object = ObjectDef {
            name: "net.xfbs.Image".to_string(),
            object: Object {
                dependencies: vec![],
                fields: [(
                    "employee".to_string(),
                    Type::Struct(Struct {
                        fields: [
                            ("name".into(), Type::Builtin(BuiltinType::String)),
                            ("age".into(), Type::Builtin(BuiltinType::Integer(None))),
                            (
                                "occupation".into(),
                                Type::Option(Box::new(Type::Builtin(BuiltinType::String))),
                            ),
                        ]
                        .into(),
                    }),
                )]
                .into(),
            },
        };
        assert_eq!(
            object
                .table_create()
                .get(0)
                .unwrap()
                .build(SqliteQueryBuilder),
            vec![
                "CREATE TABLE \"object:net.xfbs.Image\" (",
                "\"id\" integer NOT NULL PRIMARY KEY,",
                "\"field:employee:age\" integer NOT NULL,",
                "\"field:employee:name\" text NOT NULL,",
                "\"field:employee:occupation\" text )",
            ]
            .join(" "),
        );
    }
}
