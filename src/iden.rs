use sea_query::Iden;

pub struct IdenString(pub String);

impl Iden for IdenString {
    fn unquoted(&self, write: &mut dyn std::fmt::Write) {
        write.write_str(&self.0).unwrap();
    }
}
